﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FoodSystem.Models
{
    public class Food
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DisplayName("Name")]
        public string food_Name { get; set; } = string.Empty;

        [Required]
        [DisplayName("Price")]
        public decimal food_Price { get; set; }

        [Required]
        [DisplayName("Description/Ingridients")]
        public string food_Description { get; set; } = string.Empty;

        [Required]
        [DisplayName("Image")]
        public string food_ImageFileName { get; set; } = string.Empty;

        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
