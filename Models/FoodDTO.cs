﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FoodSystem.Models
{
    public class FoodDTO
    {
        [Required]
        [DisplayName("Name")]
        public string food_Name { get; set; } = string.Empty;

        [Required]
        [DisplayName("Price")]
        public decimal food_Price { get; set; }

        [Required]
        [DisplayName("Description/Ingridients")]
        public string food_Description { get; set; } = string.Empty;

        [Required]
        [DisplayName("Image")]
        public IFormFile? food_ImageFile { get; set; }
    }
}
