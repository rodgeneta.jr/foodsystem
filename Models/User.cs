﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel;

namespace FoodSystem.Models
{
    public class User : IdentityUser
    {
        [DisplayName("First name")]
        public string FirstName { get; set; } = string.Empty;

        [DisplayName("Last name")]
        public string LastName { get; set; } = string.Empty;

        [DisplayName("Home Address")]
        public string Address { get; set; } = string.Empty;

        public DateTime CreatedAt { get; set; }
    }
}
