using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FoodSystem.Pages.Cashiers
{
    [Authorize(Roles = "cashier, admin")]
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
        }
    }
}
