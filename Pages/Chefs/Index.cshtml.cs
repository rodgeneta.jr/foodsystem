using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FoodSystem.Pages.Chefs
{
    [Authorize(Roles = "chef, admin")]
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
        }
    }
}
