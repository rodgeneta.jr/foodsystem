using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FoodSystem.Pages.Chefs
{
    [Authorize(Roles = "chef, admin")]
    public class FoodManagerModel : PageModel
    {
        public void OnGet()
        {
        }
    }
}
