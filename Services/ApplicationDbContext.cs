﻿using FoodSystem.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FoodSystem.Services
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            var admin = new IdentityRole("admin");
            admin.NormalizedName = "admin";

            var cashier = new IdentityRole("cashier");
            cashier.NormalizedName = "cashier";

            var chef = new IdentityRole("chef");
            chef.NormalizedName = "chef";

            builder.Entity<IdentityRole>().HasData(admin, cashier, chef);
        }
    }
}
